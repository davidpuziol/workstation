#!/bin/bash

sudo apt-get update # Para atualizar os repositórios padrão
sudo apt-get install git curl ansible # Instale esses pacotes
ansible-galaxy collection install community.general # Instala collections que o playbook irá usar
cd /tmp
git clone https://gitlab.com/davidpuziol/workstation.git
cd workstation/ubuntu
ansible-playbook main.yaml --ask-become-pass -v # e digite a senha do seu usuáro
